<?php
//Term taxonomy ha lo status
//Term relationship le collega tramite meta_id=object_id

$servername = "areedb.polito.it";
$username = "aw210602";
$password = "GgFpwe3ysRaWQC9";
$dbname = "aw210602";
$dbtable = "wp_postmeta";

$filename="applications.csv";
$filepath = "./downloads/". $filename;

$filenameIT="applicationsIT.csv";
$filepathIT= "./downloads/". $filenameIT;

$filenameRA="applicationsRA.csv";
$filepathRA = "./downloads/". $filenameRA;

$filenameDART="applicationsDART.csv";
$filepathDART = "./downloads/". $filenameDART;

$filenameACC="applicationsACC.csv";
$filepathACC = "./downloads/". $filenameACC;

$filenameCom="applicationsCom.csv";
$filepathCom = "./downloads/". $filenameCom;


$debug = 0;

$projects= array("ACC","Comunicazione","DART","IT","RA");
?>
<html>
<head>
	<title>Electronic Inventory</title>
   <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<br>
 
<?php
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
	echo "<br>Connection failed <br>";
}

$get_areas_id = "SELECT * FROM `wp_terms`";
$result_areas_id = $conn->query($get_areas_id);

while($row_ids = $result_areas_id->fetch_assoc()) {
 	if($row_ids["name"] == "ACC"){
		$idACC=$row_ids["term_id"];
	}

 	if($row_ids["name"] == "RA"){
		$idRA=$row_ids["term_id"];
	}

 	if($row_ids["name"] == "DART"){
		$idDART=$row_ids["term_id"];
	}

 	if($row_ids["name"] == "Comunicazione"){
		$idCom=$row_ids["term_id"];
	}

 	if($row_ids["name"] == "IT"){
		$idIT=$row_ids["term_id"];
	}
	

}

if($debug){
		echo "Ids:".$idACC." ".$idRA." ".$idDART." ".$idCom." ".$idIT." <br>";
}

$sql = "SELECT * FROM `wp_postmeta` WHERE `meta_key` LIKE 'ad_id'";
$result = $conn->query($sql);


//$csv = fopen($filepath,'w');
//fputcsv($csv,array("Nome Cognome","Email","Status"));//echo "<br> yoyo <br>";

$csvACC = fopen($filepathACC,'w');
fputcsv($csvACC,array("Nome Cognome","Email","Status"));

$csvCom = fopen($filepathCom,'w');
fputcsv($csvCom,array("Nome Cognome","Email","Status"));

$csvDART = fopen($filepathDART,'w');
fputcsv($csvDART,array("Nome Cognome","Email","Status"));

$csvIT = fopen($filepathIT,'w');
fputcsv($csvIT,array("Nome Cognome","Email","Status"));

$csvRA = fopen($filepathRA,'w');
fputcsv($csvRA,array("Nome Cognome","Email","Status"));

$temp=array(array());
$accepted=array_shift($temp);
//$rejected=$temp;


	$acceptedIT=$temp;
	$pendingIT=$temp;
	$rejectedIT=$temp;

	$acceptedACC=$temp;
	$pendingACC=$temp;
	$rejectedACC=$temp;

	$acceptedRA=$temp;
	$pendingRA=$temp;
	$rejectedRA=$temp;

	$acceptedDART=$temp;
	$pendingDART=$temp;
	$rejectedDART=$temp;

	$acceptedCom=$temp;
	$pendingCom=$temp;
	$rejectedCom=$temp;



if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
	$get_application = "SELECT * FROM `wp_postmeta` WHERE `post_id` LIKE '".$row["post_id"]."'";
	if($debug){
		echo "<br>get_application:".$get_application."<br>";
	}
	$result_application = $conn->query($get_application);
	
	$get_taxonomy_id = "SELECT * FROM `wp_term_relationships` WHERE `object_id` LIKE '".$row["post_id"]."'";
	$result_taxonomy_id = $conn->query($get_taxonomy_id);
	$row_tax_id= $result_taxonomy_id->fetch_assoc();

	$get_status = "SELECT * FROM `wp_term_taxonomy` WHERE `term_id` LIKE '".$row_tax_id["term_taxonomy_id"]."'";
	$result_status = $conn->query($get_status);
	$row_status=$result_status->fetch_assoc();
	if($debug){	
		echo $row_status["description"];
	}

	while($row_application = $result_application->fetch_assoc()) {
		if($row_application["meta_key"] == "_aol_app_name" ){
			$app_name=$row_application["meta_value"];
		}
		if($row_application["meta_key"] == "_aol_app_email" ){
			$app_email=$row_application["meta_value"];
		}
		if($row_application["meta_key"] == "ad_id"){
			$get_taxonomy_id = "SELECT * FROM `wp_term_relationships` WHERE `object_id` LIKE '".$row_application["meta_value"]."'";
			$result_taxonomy_id = $conn->query($get_taxonomy_id);
			$row_tax_id= $result_taxonomy_id->fetch_assoc();
			$currentID=$row_tax_id["term_taxonomy_id"];
		} 
	}
	if($debug){
		echo "<br>ID: " . $row["post_id"]. " - Name: " .$app_name. " -Email:  " .$app_email." -Current ID: ".$currentID."<br>";
	}	
	switch($currentID){
				case $idACC:
					if($row_status["description"] == "Shortlisted Applications"){
						array_push($acceptedACC,array($app_name,$app_email,"Accettato"));
					}
					if($row_status["description"] == "Pending Applications"){
						array_push($rejectedACC,array($app_name,$app_email,"Pending"));
					}
					if($row_status["description"] == "Rejected Applications"){
						array_push($rejectedACC,array($app_name,$app_email,"Rifiutato"));
					}
					

					break;

				case $idCom:
					if($row_status["description"] == "Shortlisted Applications"){
						array_push($acceptedCom,array($app_name,$app_email,"Accettato"));
					}
					if($row_status["description"] == "Pending Applications"){
						array_push($pendingCom,array($app_name,$app_email,"Pending"));
					}
					if($row_status["description"] == "Rejected Applications"){
						array_push($rejectedCom,array($app_name,$app_email,"Rifiutato"));
					}
					break;
	
				case $idDART:
					if($row_status["description"] == "Shortlisted Applications"){
						array_push($acceptedDART,array($app_name,$app_email,"Accettato"));
					}
					if($row_status["description"] == "Pending Applications"){
						array_push($pendingDART,array($app_name,$app_email,"Pending"));
					}
					if($row_status["description"] == "Rejected Applications"){
						array_push($rejectedDART,array($app_name,$app_email,"Rifiutato"));
					}
					break;

				case $idIT:
					if($row_status["description"] == "Shortlisted Applications"){
						array_push($acceptedIT,array($app_name,$app_email,"Accettato"));
					}
					if($row_status["description"] == "Pending Applications"){
						array_push($pendingIT,array($app_name,$app_email,"Pending"));
					}
					if($row_status["description"] == "Rejected Applications"){
						array_push($rejectedIT,array($app_name,$app_email,"Rifiutato"));
					}
					break;

				case $idRA:
					if($row_status["description"] == "Shortlisted Applications"){
						array_push($acceptedRA,array($app_name,$app_email,"Accettato"));
					}
					if($row_status["description"] == "Pending Applications"){
						array_push($pendingRA,array($app_name,$app_email,"Pending"));
					}
					if($row_status["description"] == "Rejected Applications"){
						array_push($rejectedRA,array($app_name,$app_email,"Rifiutato"));
					}
					break;
				}


    }
} else {
    echo "<h1>No results :(</h1>";
}
	foreach ($acceptedACC as $fields) {
		fputcsv($csvACC, $fields);
	}
	foreach ($pendingACC as $fields) {
		fputcsv($csvACC, $fields);
	}

	foreach ($rejectedACC as $fields) {
    		fputcsv($csvACC, $fields);
	}



	foreach ($acceptedCom as $fields) {
		fputcsv($csvCom, $fields);
	}
	foreach ($pendingCom as $fields) {
    		fputcsv($csvCom, $fields);
	}
	foreach ($rejectedCom as $fields) {
    		fputcsv($csvCom, $fields);
	}




	foreach ($acceptedDART as $fields) {
		fputcsv($csvDART, $fields);
	}
	foreach ($pendingDART as $fields) {
		fputcsv($csvDART, $fields);
	}
	foreach ($rejectedDART as $fields) {
    		fputcsv($csvDART, $fields);
	}





	foreach ($acceptedIT as $fields) {
		fputcsv($csvIT, $fields);
	}
	foreach ($pendingIT as $fields) {
		fputcsv($csvIT, $fields);
	}
	foreach ($rejectedIT as $fields) {
    		fputcsv($csvIT, $fields);
	}



	foreach ($acceptedRA as $fields) {
		fputcsv($csvRA, $fields);
	}
	foreach ($pendingRA as $fields) {
		fputcsv($csvRA, $fields);
	}
	foreach ($rejectedRA as $fields) {
    		fputcsv($csvRA, $fields);
	}



	fclose($csvACC);
	fclose($csvCom);
	fclose($csvDART);
	fclose($csvIT);
	fclose($csvRA);


	$conn->close();
	//flush();
	if($debug){
	echo "<pre>".print_r($rejectedIT)."</pre>";
	echo "<pre>".print_r($acceptedIT)."</pre>";
	}
	echo "<h1><br><a href='".$filepathACC."' download>Scarica lista finale ACC</a><br></h1>";
	echo "<h1><br><a href='".$filepathCom."' download>Scarica lista finale Comunicazione</a><br></h1>";
	echo "<h1><br><a href='".$filepathDART."' download>Scarica lista finale DART</a><br></h1>";
	echo "<h1><br><a href='".$filepathIT."' download>Scarica lista finale Area IT</a><br></h1>";
	echo "<h1><br><a href='".$filepathRA."' download>Scarica lista finale RA</a><br></h1>";

/*
	header('Content-Type: application/csv'); 
	header("Content-length: " . filesize($filepath)); 
	header('Content-Disposition: attachment; filename="' . $filename. '"'); 
	echo $csv_content;
	exit();*/
?> 
<!--
</body>
</html>